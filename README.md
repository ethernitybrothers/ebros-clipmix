# EBROS CLIPMIX 4 VVVV #

Simple looping clipmixer with TouchOSC interface

# How to #
### Running ###
The mainfile is **ebros-clipmix.v4p** . Open the file in vvvv

### Configuring ###
TouchOsc ports/IPs are on the main screen.
Video clips can be selected in the Bank box.

### Operation ###
**ebros-clipmix.touchosc**